﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Washout.Data.Models;

namespace Washout.Data
{
    public class UserContext : DbContext
    {
        public UserContext() : base("name=UserContext") { }
        public UserContext(DbConnection conn) : base(conn, false) { }

        public virtual DbSet<Washer> Washers { get; set; }
        public virtual DbSet<Skill> Skills { get; set; }
        public virtual DbSet<WasherSkill> WasherSkills { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Washer>().HasKey(t => t.Id);
            modelBuilder.Entity<Skill>().HasKey(t => t.Id);
            modelBuilder.Entity<WasherSkill>().HasKey(t => t.Id);

            modelBuilder.Entity<WasherSkill>()
                .HasRequired(r => r.Washer)
                .WithMany(w => w.WasherSkills)
                .HasForeignKey(w => w.WasherId)
                .WillCascadeOnDelete();

            modelBuilder.Entity<WasherSkill>()
                .HasRequired(r => r.Skill)
                .WithMany(w => w.WasherSkills)
                .HasForeignKey(w => w.SkillId)
                .WillCascadeOnDelete();
        }
    }
}
