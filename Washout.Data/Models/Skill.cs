﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Washout.Data.Models
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<WasherSkill> WasherSkills { get; set; }

        public Skill()
        {

        }
    }
}
