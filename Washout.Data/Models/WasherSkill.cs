﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Washout.Data.Models
{
    public class WasherSkill
    {
        public int Id { get; set; }
        public int WasherId { get; set; }
        public virtual Washer Washer { get; set; }
        public int SkillId { get; set;}
        public virtual Skill Skill { get; set; }    

        public WasherSkill()
        {

        }
    }
}
