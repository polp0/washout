﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Washout.Data.Models
{
    public class Washer
    {
        public int Id { get; set; } 
        public string Name { get; set; }   
        public string Surname { get; set; }

        public virtual ICollection<WasherSkill> WasherSkills { get; set; }

        public Washer()
        {
                
        }
    }
}
