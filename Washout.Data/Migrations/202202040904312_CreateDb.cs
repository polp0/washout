﻿namespace Washout.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateDb : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Skills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WasherSkills",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        WasherId = c.Int(nullable: false),
                        SkillId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Skills", t => t.SkillId, cascadeDelete: true)
                .ForeignKey("dbo.Washers", t => t.WasherId, cascadeDelete: true)
                .Index(t => t.WasherId)
                .Index(t => t.SkillId);
            
            CreateTable(
                "dbo.Washers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Surname = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WasherSkills", "WasherId", "dbo.Washers");
            DropForeignKey("dbo.WasherSkills", "SkillId", "dbo.Skills");
            DropIndex("dbo.WasherSkills", new[] { "SkillId" });
            DropIndex("dbo.WasherSkills", new[] { "WasherId" });
            DropTable("dbo.Washers");
            DropTable("dbo.WasherSkills");
            DropTable("dbo.Skills");
        }
    }
}
