﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Washout.API.Models
{
    public class WasherModel
    {
        public int Id { get; set; }
        public string Name { get; set; }    
        public string Surname { get; set; }
        public List<SkillModel> Skills { get; set; }
    }
}