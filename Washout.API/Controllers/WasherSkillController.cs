﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Washout.API.Models;
using Washout.Data;
using Washout.Data.Models;

namespace Washout.API.Controllers
{
    /// <summary>
    /// Washer Skill Controller
    /// </summary>
    public class WasherSkillController : ApiController
    {
        private UserContext userContext { get; set; }
        public WasherSkillController()
        {
            userContext = new UserContext();
        }

        /// <summary>
        /// Assign a skill to a washer, then return washer with his skills
        /// </summary>
        /// <returns></returns>
        [Route("api/AssignSkill/{washerId}/{skillId}")]
        [ResponseType(typeof(WasherModel))]
        [HttpPost]
        public IHttpActionResult Post(int washerId, int skillId)
        {
            try
            {
                Washer washer = userContext.Washers.Where(w => w.Id == washerId).FirstOrDefault();

                if (washer != null)
                {
                    Skill skill = userContext.Skills.Where(w => w.Id == skillId).FirstOrDefault();

                    if (skill == null)
                    {
                        return NotFound();
                    }

                    if (!washer.WasherSkills.Any(a => a.Skill.Id == skill.Id))
                    {
                        userContext.WasherSkills.Add(new WasherSkill()
                        {
                            WasherId = washerId,
                            SkillId = skillId
                        });
                        userContext.SaveChanges();
                    }

                    List<SkillModel> skills = washer.WasherSkills.Select(s => new SkillModel()
                    {
                        Id = s.Skill.Id,
                        Name = s.Skill.Name
                    }).ToList();

                    return Ok(new WasherModel()
                    {
                        Id = washer.Id,
                        Name = washer.Name,
                        Surname = washer.Surname,
                        Skills = skills
                    });
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Delete a skill assignment, then return washer with his skills
        /// </summary>
        /// <returns></returns>
        [Route("api/DeleteAssignment/{washerId}/{skillId}")]
        [ResponseType(typeof(WasherModel))]
        [HttpDelete]
        public IHttpActionResult Delete(int washerId, int skillId)
        {
            try
            {
                Washer washer = userContext.Washers.Where(w => w.Id == washerId).FirstOrDefault();

                if (washer != null)
                {
                    Skill skill = userContext.Skills.Where(w => w.Id == skillId).FirstOrDefault();

                    if (skill == null)
                    {
                        return NotFound();
                    }

                    WasherSkill delete = userContext.WasherSkills.Where(a => a.Skill.Id == skill.Id && a.WasherId == washerId).FirstOrDefault();

                    if (delete != null)
                    {
                        userContext.WasherSkills.Remove(delete);
                        userContext.SaveChanges();
                    }

                    List<SkillModel> skills = washer.WasherSkills.Select(s => new SkillModel()
                    {
                        Id = s.Skill.Id,
                        Name = s.Skill.Name
                    }).ToList();

                    return Ok(new WasherModel()
                    {
                        Id = washer.Id,
                        Name = washer.Name,
                        Surname = washer.Surname,
                        Skills = skills
                    });
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
