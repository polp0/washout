﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Washout.API.Models;
using Washout.Data;
using Washout.Data.Models;

namespace Washout.API.Controllers
{
    /// <summary>
    /// Skill Controller
    /// </summary>
    public class SkillController : ApiController
    {
        private UserContext userContext { get; set; }
        public SkillController()
        {
            userContext = new UserContext();
        }

        /// <summary>
        /// Create Skill and returs it
        /// </summary>
        /// <param name="skill"></param>
        /// <returns></returns>
        [Route("api/CreateSkill")]
        [ResponseType(typeof(SkillModel))]
        [HttpPost]
        public IHttpActionResult Post(SkillModel skill)
        {
            try
            {
                Skill newSkill = new Skill()
                {
                    Name = skill.Name,

                };

                userContext.Skills.Add(newSkill);
                userContext.SaveChanges();

                return Ok(newSkill);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get Skills
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<SkillModel>))]
        [Route("api/GetSkills")]
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(userContext.Skills.Select(s => new SkillModel()
                {
                    Id = s.Id,
                    Name = s.Name,
                }).ToList());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get Skill by Id
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(SkillModel))]
        [Route("api/GetSkill/{id}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
               Skill skill = userContext.Skills
                    .Where(w => w.Id == id).FirstOrDefault();

                if (skill != null)
                {
                    return Ok(new SkillModel() { 
                        Id = skill.Id,
                        Name = skill.Name
                    });
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Update Skill and return it
        /// </summary>
        /// <returns></returns>
        [ResponseType(typeof(SkillModel))]
        [Route("api/UpdateSkill/{id}")]
        [HttpPut]
        public IHttpActionResult Put(int id, SkillModel skill)
        {
            try
            {
                Skill upd = userContext.Skills.Where(w => w.Id == id).FirstOrDefault();

                if (upd != null)
                {
                    upd.Name = skill.Name;
                    userContext.SaveChanges();

                    return Ok(new SkillModel()
                    {
                        Id = upd.Id,
                        Name = upd.Name
                    });
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }


        /// <summary>
        /// Delete Skill if not linked to any user
        /// </summary>
        /// <returns></returns>
        [Route("api/DeleteSkill/{id}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                Skill del = userContext.Skills.Where(w => w.Id == id).FirstOrDefault();

                if (del != null)
                {
                    if (del.WasherSkills.Count == 0)
                    {
                        userContext.Skills.Remove(del);
                        userContext.SaveChanges();

                        return Ok();
                    }

                    return BadRequest();
                }

                return NotFound();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
